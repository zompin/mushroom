/*
 * Библиотека для работы с параметрами
 * Алтухов Илья
 */

#include "param.h"

// Функция конструктор
// Аргументы:
// id 		- идентификатор параметра в базе
// type 	- тип параметра
// value 	- целевое значение параметра
Param::Param(unsigned short id, unsigned short roomId, uint8_t type, float value) {
	_id 			= id;
	_roomId 		= roomId;
	_type 			= type;
	_target_value 	= value;
}

// Возвращает идентификатор параметра
unsigned short Param::getParamId() {
	return _id;
}

// Возвращает идентификатор комнаты
unsigned short Param::getRoomId() {
	return _roomId;
}

// Возвращает тип параметра
uint8_t Param::getParamType() {
	return _type;
}

// Возвращает целевое значение параматра
float Param::getTargetValue() {
	return _target_value;
}

// Устанавливает текущее значение параметра
// Аргументы:
// value - новое текущее значение параметра
void Param::setCurrentValue(float value) {
	_current_value = value;
}

// Возвращает текущее значение параметра
float Param::getCurrentValue() {
	return _current_value;
}

// Включает контролы, которые включаются при понижении значения параметра ниже целевого значения
void Param::onLowControlsOn() {
	_on_low_controls_work = true;
}

// Выключает контролы, которые включаются при понижении значения параметра ниже целевого значения
void Param::onLowControlsOff() {
	_on_low_controls_work = false;
}

// Возвращает статус контролов, которые включаются понижении значения параметра ниже целевого значения
boolean Param::isOnLowControlsWork() {
	return _on_low_controls_work;
}

// Включает контролы, которые включаются при превышении значения параметра выше целевого
void Param::onHighControlsOn() {
	_on_high_controls_work = true;
}

// Выключает контролы, которые включаются при превышении значения параметра выше целевого
void Param::onHighControlsOff() {
	_on_high_controls_work = false;
}

// Возвращает статус контролов, которые включаются при превышении значения параметра выше зелевого
boolean Param::isOnHighControlsWork() {
	return _on_high_controls_work;
}

// Возвращает текущй допуск
float Param::getLimit() {
	uint8_t param_type 	= this->getParamType();
	float limit_value 	= 0;

	switch (param_type) {
		case TMP: limit_value = _temperatur_limit; 	break;
		case HUM: limit_value = _humidity_limit; 	break;
		case CO2: limit_value = _co2_limit; 		break;
	}

	return limit_value;
}

// Устанавливает новое значение допуска для параметра
void Param::setLimit(float limit) {
	uint8_t param_type = this->getParamType();

	switch (param_type) {
		case TMP: _temperatur_limit = limit; break;
		case HUM: _humidity_limit 	= limit; break;
		case CO2: _co2_limit 		= limit; break;
	}
}