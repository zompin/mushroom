/*
 * Библиотека для управления устройствами на 220В через реле
 * Алтухов Илья
 */

#ifndef CONTROL_H
#define CONTROL_H
#define ON 		1
#define OFF 	0
#define ONLOW 	1
#define ONHIGH 	2
#define HAND 	3

#if ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

class Control {
	public:
		Control(unsigned short id, unsigned short paramId, uint8_t type, uint8_t port, uint8_t timerEnable, uint32_t timerStart, uint32_t timerEnd, uint8_t timerDelay);
		void 			turnOn();
		void 			turnOff();
		unsigned short 	getControlId();
		unsigned short 	getParamId();
		uint8_t 		getControlType();
		uint8_t 		getPort();
		uint8_t 		getControlState();
		uint8_t 		isTimerEnable();
		uint32_t 		getTimerStart();
		uint32_t 		getTimerEnd();
		uint8_t 		isTimerDelay();
		void 			turnOffTimerDelay(uint32_t currentTime);

	private:
		unsigned short _id 		= 0;
		unsigned short _paramId = 0;
		uint8_t _port 			= 0;
		uint8_t _state 			= OFF;
		uint8_t _type 			= 0;
		uint8_t _timerEnable 	= 0;
		uint32_t _timerStart 	= 0;
		uint32_t _timerEnd 		= 0;
		uint8_t _timerDelay 	= 0;
};

#endif