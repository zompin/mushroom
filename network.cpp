/*
 * Модуль для работы с сетью
 * Алтухов Илья
 */

#include "network.h"

// Функция конструктор
// Аргументы:
// en_pin - номер порта к которому подключен пин CH_PD ESP8266
Esp::Esp(uint8_t en_pin) {
	_en_pin = en_pin;
	pinMode(_en_pin, OUTPUT);
	digitalWrite(_en_pin, LOW);
	Serial1.begin(9600);
}

// Берет на себя всю задачу по выполнению запроса:
// поиск модуля, подключение к точку доступа, подключение к серверу, отправку и прием данных
void Esp::request(uint8_t id) {
	unsigned long startTime = millis();
	_id = id;
	Request * tmp = getTimer(id);

	if ((startTime - tmp->timer) <= REQUEST_TIMEOUT && startTime >= REQUEST_TIMEOUT) {
		return;
	}

	if (startTime - _between_errors_timer <= ERROR_TIMEOUT) {
		return;
	}

	if (_busy && startTime - _busy_timer >= BUSY_TIMEOUT) {
		if (_do_log) {
			Serial.println(F("BUSY TIMEOUT"));
		}

		_busy = false;
		_status = ESP_NOT_FOUND;
		digitalWrite(_en_pin, HIGH);
		delay(20);
		digitalWrite(_en_pin, LOW);
		_between_errors_timer = startTime;
	}

	if (!_busy) {
		_busy_timer = startTime;
	}

	_task_timer = startTime;

	while (millis() - _task_timer < 10000 && _has_task) {
		espAction();
		espWait();

		if (_prev_status != _status && _do_log) {
			Serial.print(F("STATUS: "));
			Serial.println(_status);
			_prev_status = _status;
			Serial.print(F("FREE MEMORY: "));
			Serial.println(getFreeMemory());
		}
	}

	_has_task = true;
}

// Выполняет действие в зависимости от состояния подключения
void Esp::espAction() {
	if (!_busy) {
		switch (_status) {
			case ESP_NOT_FOUND:
				searchForEsp();
			break;
			case ESP_FOUND:
				connectToWiFi();
			break;
			case WIFI_CONNECTED:
				connectToHost();
			break;
			case HOST_CONNECTED:
				requestToHost();
			break;
			case DATA_RECIEVED:
				closeConnection();
			break;
		}
	}
}

// Ждет ответа ESP8266 и ринимает решение что делать в зависимости от ответа
void Esp::espWait() {
	char * answer 		= NULL;
	int incommingByte 	= 0;
	uint8_t reading 	= true;
	char * test 		= NULL;

	if (Serial1.available() > 0) {
		while (reading) {
			incommingByte = Serial1.read();

			if (incommingByte == -1 || incommingByte == 13) continue;
			if (incommingByte != 10) answer = scat(answer, (char) incommingByte);
			if (incommingByte == 10) reading = false;
			if (scomp(answer, "> ")) reading = false;
		}

		switch (_status) {
			case ESP_NOT_FOUND:
				waitForEsp(answer);
			break;
			case ESP_FOUND:
				waitForWiFi(answer);
			break;
			case WIFI_CONNECTED:
				waitForHost(answer);
			break;
			case HOST_CONNECTED:
				waitForRequest(answer);
			break;
			case WAIT_FOR_REQUEST:
				waitForStartRecievingData(answer);
			break;
			case RECIEVING_DATA:
				recievingData(answer);
			break;
			case DATA_RECIEVED:
				waitForClosingConnection(answer);
			break;
		}

		sclear(&answer);
	}
}

// Возвращает таймер для уазанного соединение
// Аргументы:
// id - идентификатор соединения
Request * Esp::getTimer(uint8_t id) {
	uint8_t i;

	if (_requestsCount == 0) {
		addRequest(id);
	}

	for (i = 0; i < _requestsCount; i++) {
		if (_requestsArray[i].id == id) {
			return &_requestsArray[i];
		}
	}

	addRequest(id);
}

// Создает таймер для указанного соединения
// Аргументы:
// id - идентификатор соединения
void Esp::addRequest(uint8_t id) {
	Request * tmp = NULL;
	uint8_t i;

	if (_requestsCount == 0) {
		tmp = (Request *) malloc(sizeof(Request));

		if (tmp != NULL) {
			tmp[0].id = id;
			tmp[0].timer = 0;
			_requestsArray = tmp;
			_requestsCount++;
		}
	} else {
		tmp = (Request *) malloc(sizeof(Request) * (_requestsCount + 1));

		if (tmp != NULL) {
			for (i = 0; i < _requestsCount; i++) {
				tmp[i] = _requestsArray[i];
			}

			tmp[_requestsCount].id = id;
			tmp[_requestsCount].timer = 0;
			_requestsCount++;
			free(_requestsArray);
			_requestsArray = tmp;
		}
	}
}

// Ищет ESP8266
void Esp::searchForEsp() {
	Serial1.println(F("AT"));
	_busy = true;

	if (_do_log) {
		Serial.println(F("SEARCH FOR ESP"));
	}
}

// Подключается к указанной точке доступа
void Esp::connectToWiFi() {
	Serial1.print(F("AT+CWJAP_CUR=\""));
	Serial1.print(ssid);
	Serial1.print(F("\",\""));
	Serial1.print(password);
	Serial1.println(F("\""));

	_busy = true;

	if (_do_log) {
		Serial.println(F("CONNECTING WIFI"));
	}
}

// Подключается к серверу
void Esp::connectToHost() {
	Serial1.print(F("AT+CIPSTART=\"TCP\",\""));
	Serial1.print(host);
	Serial1.print(F("\","));
	Serial1.println(port);
	_busy = true;

	if (_do_log) {
		Serial.println(F("CONNECT TO HOST"));
	}
}

// Отправляет запрос на сервер
void Esp::requestToHost() {
	unsigned short queryLength 	= 0;
	unsigned short dataLength 	= 0;
	char * cachString 			= getCacheOfString();
	char * data 				= NULL;
	char * dataLengthStr 		= NULL;

	if (method == GET) {
		_query = scat(_query, "GET ");
		_query = scat(_query, path);
		if (!_cache) _query = scat(_query, cachString);
		_query = scat(_query, " HTTP/1.1\nHost: ");
		_query = scat(_query, host);
		_query = scat(_query, "\n\n");
	}

	if (method == POST) {
		data = scat(data, "data=");

		if (slen(output) > 0) {
			data = scat(data, output);
		}

		//sclear(&output);
		dataLength = slen(data);
		dataLengthStr = uShortToString(dataLength);
		_query = scat(_query, "POST ");
		_query = scat(_query, path);
		_query = scat(_query, " HTTP/1.1\nHost: ");
		_query = scat(_query, host);
		_query = scat(_query, "\nContent-Type: application/x-www-form-urlencoded");
		_query = scat(_query, "\nContent-Length: ");
		_query = scat(_query, dataLengthStr);
		_query = scat(_query, "\n\n");
		_query = scat(_query, data);

		sclear(&data);
		sclear(&dataLengthStr);
	}
	
	sclear(&cachString);

	queryLength = slen(_query);
	Serial1.print(F("AT+CIPSEND="));
	Serial1.println(queryLength);
	_busy = true;

	if (_do_log) {
		Serial.println(F("HOST REQUEST"));
		Serial.println(_query);
	}
}

// Закрывает соединение
void Esp::closeConnection() {
	_busy = true;
	Serial1.println(F("AT+CIPCLOSE"));

	if (_do_log) {
		Serial.println(F("CLOSING CONNECTION"));
	}
}

// Ожидает ответ от ESP8266 о доступности
// Аргументы:
// answer - строка ответа от ESP8266
void Esp::waitForEsp(char * answer) {
	if (scomp(answer, "OK") || scomp(answer, "ready")) {
		_busy = false;
		_status = ESP_FOUND;
		_task_timer = millis();

		if (_do_log) {
			Serial.println(F("ESP FOUND"));
		}
	}

	if (scomp(answer, "SEND FAIL")) {
		_busy = false;
		_status = ESP_NOT_FOUND;
		_task_timer = millis();
		_between_errors_timer = _task_timer;
	}
}

// Ожидает ответ о подключении или неподключении к точке доступа
// Аргументы:
// answer - строка ответа от ESP8266
void Esp::waitForWiFi(char * answer) {
	if (scomp(answer, "OK")) {
		_busy = false;
		_status = WIFI_CONNECTED;

		if (_do_log) {
			Serial.println(F("WIFI CONNECTED"));
		}
	}

	if (scomp(answer, "FAIL")) {
		_busy = false;
		_status = ESP_FOUND;
		_has_task = false;
		_between_errors_timer = millis();

		if (_do_log) {
			Serial.println(F("WIFI CONNECT FAIL"));
		}
	}
}

// Ожидает ответ от сервера на подключение
// Аргументы:
// answer - строка ответа от ESP8266
void Esp::waitForHost(char * answer) {
	if (scomp(answer, "CONNECT")) {
		_status = HOST_CONNECTED;
		_busy = false;
		_task_timer = millis();

		if (_do_log) {
			Serial.println(F("CONNECTED TO HOST"));
		}
	}
}

// Ожидает приглашения для выполнения запроса
// Аргументы:
// answer - строка ответа от ESP8266
void Esp::waitForRequest(char * answer) {
	if (scomp(answer, "> ")) {
		Serial1.println(_query);
		sclear(&_query);
		_status 	= WAIT_FOR_REQUEST;
		_task_timer = millis();
	}
}

// Ожидает ответа об успешной отправки запроса
// Аргументы:
// answer - строка ответа от ESP8266
void Esp::waitForStartRecievingData(char * answer) {
	if (scomp(answer, "SEND OK")) {
		_status = RECIEVING_DATA;
		_task_timer = millis();

		if (_do_log) {
			Serial.println(F("REQUEST COMPLETED"));
			Serial.println(F("RECIEVING DATA"));
		}
	}
}

// Принимает данные с сервера (только тело ответа, обрамленное тегами)
// Аргументы:
// answer - строка ответа от ESP8266
void Esp::recievingData(char * answer) {
	if (scomp(answer, "0")) {
		if (close_connection_between_requests) {
			_status = DATA_RECIEVED;
		} else {
			_status = HOST_CONNECTED;
			_has_task = false;
		}

		_busy = false;

		if (_do_log) {
			Serial.println(F("DATA RECIEVED"));
		}
	} else {
		if (_do_log) {
			Serial.println(answer);
		}

		if (scomp(answer, "data--")) {
			_write_to_buffer = false;
		}

		if (_write_to_buffer) {
			input = scopy(answer);
		}

		if (scomp(answer, "--data")) {
			_write_to_buffer = true;
		}
		_task_timer = millis();
	}

	if (scomp(answer, "CLOSED")) {
		_status = WIFI_CONNECTED;
		_has_task = false;
		_busy = false;
	}
}

// Ожидает ответа на закрытие соединения
// Аргументы:
// answer - строка ответа от ESP8266
void Esp::waitForClosingConnection(char * answer) {
	Request *tmp = getTimer(_id);

	if (scomp(answer, "CLOSED")) {
		_busy = false;
		_status = WIFI_CONNECTED;
		_has_task = false;
		//_task_timer = millis();
		tmp->timer = millis();
		if (_do_log) {
			Serial.println(F("CONNECTION CLOSED"));
		}
	}
}

// Возвращает текущий статус соединения
uint8_t Esp::getStatus() {
	return _status;
}

// Возращает строку для игнорирования кэша
char * Esp::getCacheOfString() {
	char * s = NULL;
	char * n = uShortToString(random(0, 65535));

	if (indexOf(path, '?') == -1)  {
		s = scat(s, "?cacheof=");
	} else {
		s = scat(s, "&cacheof=");
	}
	
	s = scat(s, n);
	sclear(&n);

	return s;
}

// Включает вывод отладочной информации
void Esp::enableLog() {
	_do_log = true;
}

// Отключает вывод отладочной информации
void Esp::disableLog() {
	_do_log = false;
}

void Esp::toggleLog() {
	_do_log = !_do_log;
}

// Производит расчет контрольной суммы по алгоритму CRC16
// Аргументы:
// str - строка для которой нужно вычислить контрольную сумму
unsigned short Esp::crc16(const char * str) {
	unsigned short crc 	= 0xffff;
	unsigned short poly = 0x1021;
	unsigned short len 	= slen(str);
	char i 				= 0;
	unsigned short j 	= 0;

	while (len--) {
		crc ^= str[j] << 8;

		for (i = 0; i < 8; i++) {
			if (crc & 0x8000) {
				crc <<= 1;
				crc ^= 0x1021;
			} else {
				crc <<= 1;
			}
		}
		j++;
	}

	return crc;
}