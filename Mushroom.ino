/*
 * Прошивка для системы управления климатом
 * Алтухов Илья
 */

#include "mushroom.h"
#include "constants-cp1251.h"

Param   * 		paramArray;
Sensor  * 		sensorArray;
Control * 		controlArray;
ParamBackup * 	paramBackupArray;
ControlBackup * controlBackupArray;
uint8_t 		paramsCount;
uint8_t 		sensorsCount;
uint8_t 		controlsCount;
uint8_t 		paramBackupCount 	= 0;
uint8_t 		controlBackupCount 	= 0;
uint8_t 		sensorMode 			= PROD;
unsigned short 	currHashes[3] 		= {0};
char * 			paths[5] 			= {
	"/action.php?action=get_hashs",
	"/action.php?action=get_params",
	"/action.php?action=get_sensors",
	"/action.php?action=get_controls",
	"/action.php"
};
volatile uint8_t waitFor 			= NONE;

DB db(K4);
Esp esp(48);
DS3231 clock;
RTCDateTime dt;

void setup() {
	Serial.begin(115200);
	Serial2.begin(9600);
	clock.begin();
	//esp.ssid 					= "Magister-Yoda";
	//esp.password 				= "7252489067";
	//esp.ssid 					= "21";
	//esp.password 				= "lena1234";
	//esp.ssid 					= "Parallax";
	//esp.password 				= "11042016mpp";
	//esp.host 					= "mtp.onehead.kz";
	//esp.host 					= "192.168.1.50";
	paramArray 					= db.getParams();
	sensorArray 				= db.getSensors();
	controlArray 				= db.getControls();
	paramsCount 				= db.getParamsCount();
	sensorsCount 				= db.getSensorsCount();
	controlsCount 				= db.getControlsCount();
	currHashes[PARAM_HASH] 		= db.getHash(PARAM_HASH_BYTE);
	currHashes[SENSOR_HASH] 	= db.getHash(SENSOR_HASH_BYTE);
	currHashes[CONTROL_HASH] 	= db.getHash(CONTROL_HASH_BYTE);
	db.getString(&esp.ssid, START_SSID_BYTE, END_SSID_BYTE);
	db.getString(&esp.password, START_PASS_BYTE, END_PASS_BYTE);
	db.getString(&esp.host, START_HOST_BYTE, END_HOST_BYTE);
	Serial.println(F(START));
}

void loop() {
	Serial2.print('w');
	getData();
	sendData();
	lookForParametr(TMP);
	lookForParametr(HUM);
	lookForParametr(CO2);
	dt = clock.getDateTime();
	//delay(500);
}

// Запрашивает информацию с сервера
void getData() {
	char * 				data 		= NULL;
	unsigned short * 	inputHashes = NULL;

	esp.path 	= paths[0];
	esp.method 	= GET;
	esp.request(HASHES_REQUEST);

	if (isStringCorrect(esp.input)) {
		data 		= getDataFromString(esp.input);
		inputHashes = explodeHashes(data);
		sclear(&esp.input);
		sclear(&data);
		lookForChanges(inputHashes);
		free(inputHashes);
	} else {
		sclear(&esp.input);
	}
}

// Функция отсылает актуальные данные на сервер
void sendData() {
	char * params 	= serializeParams();
	char * sensors 	= serializeSensors();
	char * controls = serializeControls();
	char * buf 		= NULL;
	char * id 		= NULL;
	char * value 	= NULL;
	char * state 	= NULL;
	char * hashStr	= NULL;
	char * memoryS 	= NULL;

	buf 	= scat(buf, params);
	sclear(&params);
	buf 	= scat(buf, '|');
	buf 	= scat(buf, sensors);
	sclear(&sensors);
	buf 	= scat(buf, '|');
	buf 	= scat(buf, controls);
	sclear(&controls);
	buf 	= scat(buf, '|');
	memoryS = uShortToString((unsigned short) getFreeMemory());
	buf 	= scat(buf, memoryS);
	sclear(&memoryS);
	hashStr = uShortToString(esp.crc16(buf));
	buf 	= scat(buf, ';');
	buf 	= scat(buf, hashStr);
	buf 	= scat(buf, "&action=insert_data");

	esp.path 	= paths[4];
	esp.output 	= buf;
	esp.method 	= POST;
	
	esp.request(SEND_DATA_REQUEST);
	sclear(&esp.input);
	sclear(&hashStr);
	sclear(&buf);
}

// Сериализует параметры
char * serializeParams() {
	Param * param;
	char * 	id 		= NULL;
	char * 	value 	= NULL;
	char * 	buf 	= NULL;
	uint8_t i;

	for (i = 0; i < paramsCount; i++) {
		param 	= &paramArray[i];
		id 		= uShortToString(param->getParamId());
		value 	= floatToString(param->getCurrentValue());
		buf 	= scat(buf, id);
		buf 	= scat(buf, ',');
		buf 	= scat(buf, value);

		if (i < paramsCount - 1) {
			buf = scat(buf, ':');
		}

		sclear(&id);
		sclear(&value);
	}

	return buf;
}

// Сериализует датчики
char * serializeSensors() {
	Sensor * sensor;
	char * 	id 		= NULL;
	char * 	value 	= NULL;
	char * 	buf 	= NULL;
	uint8_t i;

	for (i = 0; i < sensorsCount; i++) {
		sensor 	= &sensorArray[i];
		id 		= uShortToString(sensor->getSensorId());
		value 	= floatToString(sensor->getValue());
		buf 	= scat(buf, id);
		buf 	= scat(buf, ',');
		buf 	= scat(buf, value);

		if (i < sensorsCount - 1) {
			buf = scat(buf, ':');
		}

		sclear(&id);
		sclear(&value);
	}

	return buf;
}

// Сериализует контролы
char * serializeControls() {
	Control * control;
	char * 	id 			= NULL;
	char * 	state 		= NULL;
	char *	timerDelay 	= NULL;
	char * 	buf 		= NULL;
	uint8_t i;

	for (i = 0; i < controlsCount; i++) {
		control 	= &controlArray[i];
		id 			= uShortToString(control->getControlId());
		state 		= uShortToString((unsigned short) control->getControlState());
		timerDelay 	= uShortToString((unsigned short) control->isTimerDelay());
		buf 		= scat(buf, id);
		buf 		= scat(buf, ',');
		buf 		= scat(buf, state);
		buf 		= scat(buf, ',');
		buf 		= scat(buf, timerDelay);

		if (i < controlsCount - 1) {
			buf = scat(buf, ':');
		}

		sclear(&id);
		sclear(&state);
		sclear(&timerDelay);
	}

	return buf;
}

// Проверяет корректна ли строка
// Аргументы:
// str - строка для проверки
boolean isStringCorrect(const char * str) {
	char * 			data 		= NULL;
	unsigned short 	hash 		= 0;
	unsigned short 	currHash 	= 0;

	if (slen(str) == 0) return false;

	hash 		= getHashFromString(str);
	data 		= getDataFromString(str);
	currHash 	= esp.crc16(data);
	free(data);

	return currHash == hash;
}

// Следит за изменением данных на сервере, если изменились, заносит их в базу контроллера
// Аргументы:
// str - строка с контрольными суммами
void lookForChanges(const unsigned short * inputHashes) {
	updateRecords(inputHashes, PARAM_HASH);
	updateRecords(inputHashes, SENSOR_HASH);
	updateRecords(inputHashes, CONTROL_HASH);

	return;
}

// Обновляет записи в базе, если они изменились на сервере
// Аргументы:
// hashes - контрольные суммы полученные с сервера
// hashType - тип проверяемой контрольной суммы
void updateRecords(const unsigned short * hashes, uint8_t hashType) {
	char * data = NULL;

	if (currHashes[hashType] != hashes[hashType]) {
		switch (hashType) {
			case PARAM_HASH:
				esp.path = paths[1];
			break;
			case SENSOR_HASH:
				esp.path = paths[2];
			break;
			case CONTROL_HASH:
				esp.path = paths[3];
			break;
		}

		esp.request(GET_CONFIG_REQUEST);

		if (isStringCorrect(esp.input)) {
			data 					= getDataFromString(esp.input);
			sclear(&esp.input);
			currHashes[hashType] 	= hashes[hashType];
			
			switch (hashType) {
				case PARAM_HASH:
					updateParams(data, hashes[hashType]);
				break;
				case SENSOR_HASH:
					updateSensors(data, hashes[hashType]);
				break;
				case CONTROL_HASH:
					updateControls(data, hashes[hashType]);
				break;
			}

			free(data);
		}
	}

	sclear(&esp.input);
}

// Обновляет параметры в БД и загружает в память
void updateParams(char * data, unsigned short hash) {
	backupParams();
	free(paramArray);
	db.addParamsFromString(data);
	db.addHash(PARAM_HASH_BYTE, hash);
	paramArray 				= db.getParams();
	paramsCount 			= db.getParamsCount();
	currHashes[PARAM_HASH] 	= hash;
	alignParams();
}

// Обновляет датчики в БД и загружает в память
void updateSensors(char * data, unsigned short hash) {
	free(sensorArray);
	db.addSensorsFromString(data);
	db.addHash(SENSOR_HASH_BYTE, hash);
	sensorArray 				= db.getSensors();
	sensorsCount 				= db.getSensorsCount();
	currHashes[SENSOR_HASH] 	= hash;
}

// Обновляет контролы в БД и загружает в память
void updateControls(char * data, unsigned short hash) {
	backupControls();
	free(controlArray);
	db.addControlsFromString(data);
	db.addHash(CONTROL_HASH_BYTE, hash);
	controlArray 				= db.getControls();
	controlsCount 				= db.getControlsCount();
	currHashes[CONTROL_HASH] 	= hash;
	alignControls();
}

// Разбивает строки с контрольными суммами на строки
// str - строка с контрольными суммами
unsigned short * explodeHashes(const char * str) {
	size_t len 				= slen(str);
	size_t i 				= 0;
	uint8_t index 			= 0;
	uint8_t count 			= 1;
	char * buf 				= NULL;
	unsigned short * res 	= NULL;

	if (len > 0) {
		for (i = 0; i < len; i++) {
			if (str[i] == ':') {
				count++;
			}
		}
	}

	res = (unsigned short *) malloc(sizeof(unsigned short) * count);

	if (res == NULL) return NULL;

	for (i = 0; i < len; i++) {
		if (str[i] != ':') {
			buf = scat(buf, str[i]);
		} else {
			res[index] = atol(buf);
			index++;
			free(buf);
			buf = NULL;
		}
	}

	res[index] = atol(buf);
	free(buf);
	buf = NULL;

	return res;
}

// Отделяет данные от контрольной суммы
// Аргументы:
// str - исходная строка от которой будет отделена контрольная сумма
char * getDataFromString(const char *str) {
	size_t 	len 			= slen(str);
	size_t 	charNumber 		= 0;
	char *	buf 			= NULL;

	if (len > 0) {
		while (str[charNumber] != ';') {
			buf = scat(buf, str[charNumber]);
			charNumber++;
		}
	}

	return buf;
}
/*char * getDataFromString(char * str) {
	size_t len = slen(str);
	size_t pos = indexOf(str, ';');

	if (str != NULL) {
		str[pos] = '\0';
		str = (char *) realloc(str, pos + 1);
	}

	return str;
}*/

// Отделяет контрольную сумму от строки
// Аргументы:
// str - строка, из которой нужно извлечь контрольную сумму
unsigned short getHashFromString(const char *str) {
	char * 			buffer 		= NULL;
	size_t 			len 		= slen(str);
	size_t 			i 			= 0;
	boolean 		hashStart 	= false;
	char 			ch 			= 0;
	unsigned short 	hash 		= 0;

	for (i = 0; i < len; i++) {
		ch = str[i];
		if (hashStart) {
			buffer = scat(buffer, ch);
		}
		if (ch == ';') {
			hashStart = true;
		}
	}

	hash = (unsigned short) atol(buffer);
	free(buffer);

	return hash;
}

// Следит за параметрами указанного типа
// Аргументы:
// paramType - тип параметра
void lookForParametr(uint8_t paramType) {
	uint8_t i;
	float 	currentValue;
	Param *	param;
	Param *	tmpParam 	= NULL;
	Param *	humParam 	= NULL;
	float 	tmp 		= NAN;
	float 	hum 		= NAN;

	for (i = 0; i < paramsCount; i++) {
		param = &paramArray[i];

		if (param->getParamType() == paramType) {
			if (paramType == CO2) {
				tmpParam = getParamByTypeAndRoomId(TMP, param->getRoomId());
				humParam = getParamByTypeAndRoomId(HUM, param->getRoomId());

				if (tmpParam != NULL) tmp = tmpParam->getCurrentValue();
				if (humParam != NULL) hum = humParam->getCurrentValue();
				
				if (tmp == tmp && hum == hum) {
					currentValue = getCurrentParamValue(param, tmp, hum);
				} else {
					currentValue = getCurrentParamValue(param);
				}
			} else {
				currentValue = getCurrentParamValue(param);
			}
			
			param->setCurrentValue(currentValue);

			if (currentValue == currentValue) {
				controlRullingByTime();
				delayControlRulling(param);
				onLowControlRuling(param);
				onHighControlRuling(param);
			} else {
				controlsTurnOff(param, ONLOW);
				controlsTurnOff(param, ONHIGH);
				param->onLowControlsOff();
				param->onHighControlsOff();
			}
		}
	}
}

// Возвращает текущее значение параметра
// Аргументы:
// param - параметр для которого нужно нужно вернуть текущее значение
float getCurrentParamValue(Param * param, float tmp, float hum) {
	float value 					= 0;
	float buffer 					= 0;
	uint8_t countOfTargetSensors 	= 0;
	Sensor *sensor;

	for (byte i = 0; i < sensorsCount; i++) {
		sensor = &sensorArray[i];

		if (sensor->isEnable() && sensor->getParamId() == param->getParamId())  {	
			if (param->getParamType() == CO2) {
				sensor->updateValue(param->getParamType(), tmp, hum);
			} else {
				sensor->updateValue(param->getParamType());
			}

			if (sensorMode == TEST) {
				if (param->getParamType() == CO2) {
					sensor->setValue((float) analogRead(A15));
				} else {
					sensor->setValue((float) analogRead(A15) / 10);
				}

				Serial.print("Test value: ");
				Serial.println(sensor->getValue());
			}

			buffer = sensor->getValue();

			if (buffer == buffer) {
				countOfTargetSensors++;
				value += buffer;
			}
		}
	}

	if (value != 0 && countOfTargetSensors != 0) {
		value /= countOfTargetSensors;
	} else {
		value = NAN;
	}

	return value;
}

// Управляет контролами, которые включаются при значении параметра ниже целевого
// Аргументы:
// param - параметр, которому принадлежать управляемые контролы
void onLowControlRuling(Param * param) {
	float limit 		= param->getLimit();
	float targetValue 	= param->getTargetValue();
	float currentValue 	= param->getCurrentValue();

	if (param->isOnLowControlsWork()) {
		//if (currentValue > targetValue + limit) {
		//if (currentValue > targetValue + limit / 2) {
		if (currentValue > targetValue) {
			controlsTurnOff(param, ONLOW);
		}
	} else {
		if (currentValue < targetValue - limit) {
			controlsTurnOn(param, ONLOW);
		}
	}
}

// Управляет контролами, которые включаются при значении параметра выше целевого
// Аргументы:
// param - параметр, которому принадлежать управляемые контролы
void onHighControlRuling(Param * param) {
	float limit 		= param->getLimit();
	float targetValue 	= param->getTargetValue();
	float currentValue 	= param->getCurrentValue();

	if (param->isOnHighControlsWork()) {
		//if (currentValue < targetValue - limit) {
		//if (currentValue < targetValue - limit / 2) {
		if (currentValue < targetValue) {
			controlsTurnOff(param, ONHIGH);
		}
	} else {
		if (currentValue > targetValue + limit) {
			controlsTurnOn(param, ONHIGH);
		}
	}
}

// Следит за тем, чтобы контролы были включены только в определенное время
void controlRullingByTime() {
	uint32_t 	timerStart;
	uint32_t 	timerEnd;
	uint32_t 	unixtime = dt.unixtime;
	uint8_t 	i;
	uint8_t 	controlType;
	uint8_t 	onLowControlState;
	uint8_t 	onHighControlState;
	uint16_t 	paramId;
	Control * 	control;
	Param * 	param;

	for (i = 0; i < controlsCount; i++) {
		control 	= &controlArray[i];

		if (!control->isTimerEnable() || control->isTimerDelay()) {
			continue;
		}

		paramId 	= control->getParamId();
		timerStart 	= control->getTimerStart();
		timerEnd 	= control->getTimerEnd();
		controlType = control->getControlType();
		param 		= getParamById(paramId);

		if (param) {
			onLowControlState = param->isOnLowControlsWork();
			onHighControlState = param->isOnHighControlsWork();
		}

		if (unixtime < timerStart || unixtime > timerEnd) {
			control->turnOff();
		} else {
			if (param) {
				if (onLowControlState && controlType == ONLOW) {
					control->turnOn();
				}

				if (onHighControlState && controlType == ONHIGH) {
					control->turnOn();
				}
			}
		}
	}
}

void delayControlRulling(Param * param) {
	float limit 		= param->getLimit();
	float targetValue 	= param->getTargetValue();
	float currentValue 	= param->getCurrentValue();
	uint8_t i;
	unsigned short id;

	if (param->isOnHighControlsWork() || param->isOnLowControlsWork()) return;

	if (currentValue < targetValue + limit && currentValue > targetValue - limit) {
		for (i = 0; i < controlsCount; i++) {
			if (controlArray[i].isTimerDelay()) {
				id = controlArray[i].getControlId();
				controlArray[i].turnOffTimerDelay(dt.unixtime);
				db.updateControl(id, &controlArray[i]);
			}
		}
	}
}

// Возвращает параметр по id;
// Аргументы:
// id - идентификатор параметра
Param * getParamById(unsigned short id) {
	uint8_t i;
	Param * param;

	for (i = 0; i < paramArray; i++) {
		param = &paramArray[i];

		if (param->getParamId() == id) {
			return param;
		}
	}

	return NULL;
}

// Включает контролы принадлежащие параметру
// Аргументы:
// param 	   - параметр которому принадлежать котроллеры для выключения
// controlType - тип контрола
void controlsTurnOn(Param * param, uint8_t controlType) {
	uint8_t 	i;
	uint32_t 	timerStart;
	uint32_t 	timerEnd;
	uint32_t 	unixtime = dt.unixtime;
	Control * 	control;
	
	for (i = 0; i < controlsCount; i++) {
		control = &controlArray[i];

		if (control->getControlType() == controlType && control->getParamId() == param->getParamId()) {
			//if (control->isTimerEnable()) {
			if (control->isTimerEnable() && !control->isTimerDelay()) {
				timerStart 	= control->getTimerStart();
				timerEnd 	= control->getTimerEnd();

				if (unixtime > timerStart && unixtime < timerEnd) {
					control->turnOn();
				} else {
					control->turnOff();
				}
			} else {
				control->turnOn();
			}
		}
	}

	switch (controlType) {
		case ONHIGH: param->onHighControlsOn();
		break;
		case ONLOW: param->onLowControlsOn();
		break;
	}
}

// Выключает контролы принадлежащие параметру
// Аргументы:
// param 	   - параметр которому принадлежать котроллеры для включения
// controlType - тип контрола
void controlsTurnOff(Param * param, uint8_t controlType) {
	uint8_t i;
	unsigned short id;
	Control * control;

	for (i = 0; i < controlsCount; i++) {
		control = &controlArray[i];
		if (control->getControlType() == controlType && control->getParamId() == param->getParamId()) {
			control->turnOff();
		}
	}

	switch (controlType) {
		case ONHIGH: param->onHighControlsOff();
		break;
		case ONLOW: param->onLowControlsOff();
		break;
	}
}

Param * getParamByTypeAndRoomId(uint8_t type, unsigned short roomId) {
	uint8_t i;

	for (i = 0; i < paramsCount; i++) {
		if (paramArray[i].getParamType() == type && paramArray[i].getRoomId() == roomId) {
			return &paramArray[i];
		}
	}

	return NULL;
}

// Обработчик прерывания последовательного порта, обрабатывает команды
void serialEvent() {
	String command;
	char * data = NULL;

	if (Serial.available() > 0) {
		command = Serial.readStringUntil('\n');
		command.replace("\r", "");
	} else {
		return;
	}

	if (waitFor != NONE) {
		writeEnteredData(command.c_str());
		return;
	}

	if (command == "help") {
		Serial.println(F(HELP_DESC));
		Serial.println(F(VERSION_DESC));
		Serial.println(F(SENST_DESC));
		Serial.println(F(SENSP_DESC));
		Serial.println(F(ERASES_DESC));
		Serial.println(F(ERASEF_DESC));
		Serial.println(F(PARAMS_DESC));
		Serial.println(F(SENSORS_DESC));
		Serial.println(F(CONTROLS_DESC));
		Serial.println(F(SETSSID_DESC));
		Serial.println(F(GETSSID_DESC));
		Serial.println(F(SETPASSWORD));
		Serial.println(F(GETPASSWORD));
		Serial.println(F(SETHOST));
		Serial.println(F(GETHOST));
		Serial.println(F(SETSTAMP));
		Serial.println(F(AUTHOR));
	} else if (command == "version") {
		Serial.println(F(FIRMWARE_VERSION));
	} else if (command == "reset") {
		Serial.println(F("RESET_MESSAGE"));
		// Здесь должна быть команда для перезагрузки
	} else if (command == "senst") {
		sensorMode = TEST;
		Serial.println(F(SENSOR_TEST_MESSAGE));
	} else if (command == "sensp") {
		sensorMode = PROD;
		Serial.println(F(SENSOR_PROD_MESSAGE));
	} else if (command == "erases") {
		Serial.println(F(ERASE_ALL_MESSAGE));
		db.erase(ALL);
		currHashes[PARAM_HASH] = 0;
		currHashes[SENSOR_HASH] = 0;
		currHashes[CONTROL_HASH] = 0;
		Serial.println(F(ERASE_FINISH_MESSAGE));
	} else if (command == "erasef") {
		Serial.println(F(ERASE_FULL_MESSAGE));
		db.erase(FULL);
		currHashes[PARAM_HASH] = 0;
		currHashes[SENSOR_HASH] = 0;
		currHashes[CONTROL_HASH] = 0;
		Serial.println(F(ERASE_FINISH_MESSAGE));
	} else if (command == "params") {
		Serial.println(F(PARAMS_MESSAGE));
		db.printParams(paramArray, paramsCount);
	} else if (command == "sensors") {
		Serial.println(F(SENSORS_MESSAGE));
		db.printSensors(sensorArray, sensorsCount);
	} else if (command == "controls") {
		Serial.println(F(CONTROL_MESSAGE));
		db.printControls(controlArray, controlsCount);
	} else if (command == "setssid") {
		Serial.println(F(ENTER_SSID_MESSAGE));
		waitFor = SSID;
	} else if (command == "setpass") {
		Serial.println(F(ENTER_PASS_MESSAGE));
		waitFor = PASS;
	} else if (command == "sethost") {
		Serial.println(F(ENTER_HOST_MESSGAGE));
		waitFor = HOST;
	} else if (command == "getssid") {
		db.getString(&data, START_SSID_BYTE, END_SSID_BYTE);
		Serial.print(F(SSID_MESSAGE));
		Serial.println(data);
	} else if (command == "getpass") {
		db.getString(&data, START_PASS_BYTE, END_PASS_BYTE);
		Serial.print(F(PASSWORD_MESSAGE));
		Serial.println(data);
	} else if (command == "gethost") {
		db.getString(&data, START_HOST_BYTE, END_HOST_BYTE);
		Serial.print(F(HOST_MESSAGE));
		Serial.println(data);
	} else if (command == "setstamp") {
		Serial.print(F(TIMESTAM_MESSAGE));
		waitFor = TIME;
	} else if (command == "netlog") {
		esp.toggleLog();
	} else {
		Serial.print(F(UNKNOWN_COMMAND));
		Serial.println(command);
	}

	sclear(&data);
}

// Записывает введенные данные
void writeEnteredData(char * data) {
	switch (waitFor) {
		case SSID:
			Serial.print(F(SSID_MESSAGE));
			db.writeString(data, START_SSID_BYTE, END_SSID_BYTE);
			sclear(&esp.ssid);
			db.getString(&esp.ssid, START_SSID_BYTE, END_SSID_BYTE);
		break;
		case PASS:
			Serial.print(F(PASSWORD_MESSAGE));
			db.writeString(data, START_PASS_BYTE, END_PASS_BYTE);
			sclear(&esp.password);
			db.getString(&esp.password, START_PASS_BYTE, END_PASS_BYTE);
		break;
		case HOST:
			Serial.print(F(HOST_MESSAGE));
			db.writeString(data, START_HOST_BYTE, END_HOST_BYTE);
			sclear(&esp.host);
			db.getString(&esp.host, START_HOST_BYTE, END_HOST_BYTE);
		break;
		case TIME:
			clock.setDateTime(toULong(data));
			Serial.println(toULong(data));
		break;
	}

	Serial.println(data);
	waitFor = NONE;
}

// Делает резервную копию необходимых свойств параметра
void backupParams() {
	uint8_t i;
	paramBackupArray = (ParamBackup *) malloc(sizeof(ParamBackup) * paramsCount);
	paramBackupCount = paramsCount;

	for (i = 0; i < paramsCount; i++) {
		paramBackupArray[i].id 						= paramArray[i].getParamId();
		paramBackupArray[i].isOnLowControlsWork 	= paramArray[i].isOnLowControlsWork();
		paramBackupArray[i].isOnHighControlsWork 	= paramArray[i].isOnHighControlsWork();
	}
}

// Делает резервную копию необходимых свойств контрола
void backupControls() {
	uint8_t i;
	controlBackupArray = (ControlBackup *) malloc(sizeof(ControlBackup) * controlsCount);
	controlBackupCount = controlsCount;

	for (i = 0; i < controlsCount; i++) {
		controlBackupArray[i].port 		= controlArray[i].getPort();
		controlBackupArray[i].type 		= controlArray[i].getControlType();
		controlBackupArray[i].paramId 	= controlArray[i].getParamId();
	}
}

// Воостанавливает состояния параметров
void alignParams() {
	uint8_t i;
	uint8_t j;

	for (i = 0; i < paramBackupCount; i++) {
		for (j = 0; j < paramsCount; j++) {
			if (paramBackupArray[i].id == paramArray[j].getParamId()) {

				if (paramBackupArray[i].isOnLowControlsWork) {
					paramArray[j].onLowControlsOn();
				} else {
					paramArray[j].onLowControlsOff();
				}

				if (paramBackupArray[i].isOnHighControlsWork) {
					paramArray[j].onHighControlsOn();
				} else {
					paramArray[j].onHighControlsOff();
				}

				break;
			}
		}
	}

	free(paramBackupArray);
	paramBackupCount = 0;
}

// Выравнивает состояния контролов
void alignControls() {
	uint8_t i;
	uint8_t j;
	uint8_t controlType;
	boolean isControlFound;

	for (i = 0; i < controlBackupCount; i++) {
		isControlFound = false;
		for (j = 0; j < controlsCount; j++) {
			if (controlBackupArray[i].port == controlArray[j].getPort()) {
				break;
			}
		}

		if (!isControlFound) {
			digitalWrite(controlBackupArray[i].port, LOW);
		}
	}

	for (i = 0; i < controlBackupCount; i++) {
		isControlFound = false;
		for (j = 0; j < paramsCount; j++) {
			if (controlBackupArray[i].paramId == paramArray[j].getParamId()) {
				break;
			}
		}

		if (!isControlFound) {
			digitalWrite(controlBackupArray[i].port, LOW);
		}
	}

	for (i = 0; i < controlsCount; i++) {
		for (j = 0; j < paramsCount; j++) {
			if (controlArray[i].getParamId() == paramArray[j].getParamId()) {
				controlType = controlArray[i].getControlType();
				switch (controlType) {
					case ONLOW:
						if (paramArray[j].isOnLowControlsWork()) {
							controlArray[i].turnOn();
						} else {
							controlArray[i].turnOff();
						}
					break;
					case ONHIGH:
						if (paramArray[j].isOnHighControlsWork()) {
							controlArray[i].turnOn();
						} else {
							controlArray[i].turnOff();
						}
					break;
				}
			}
		}
	}

	free(controlBackupArray);
	controlBackupCount = 0;
}