/*
 * Библиотека для работы с параметрами
 * Алтухов Илья
 */

#ifndef PARAM_H
#define PARAM_H
#define HUM 1
#define TMP 2
#define CO2 3

#if ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

class Param {
	public:
		Param(unsigned short id, unsigned short roomId, uint8_t type, float value);
		unsigned short 	getParamId();
		unsigned short 	getRoomId();
		uint8_t 		getParamType();
		float   		getTargetValue();
		void			setCurrentValue(float value);
		float			getCurrentValue();
		void 			onLowControlsOn();
		void 			onLowControlsOff();
		boolean 		isOnLowControlsWork();
		void 			onHighControlsOn();
		void 			onHighControlsOff();
		boolean 		isOnHighControlsWork();
		float			getLimit();
		void			setLimit(float limit);

	private:
		unsigned short _id 				= 0;
		unsigned short _roomId 			= 0;
		uint8_t _type 					= 0;
		float   _target_value 			= 0;
		float	_current_value 			= NAN;
		boolean _on_low_controls_work 	= false;
		boolean _on_high_controls_work 	= false;
		float	_temperatur_limit 		= 0.5;
		float	_humidity_limit 		= 2;
		float	_co2_limit 				= 25;
};

#endif