/*
 * Модуль для работы с базой данных контроллера
 * Алтухов Илья
 */
#ifndef DB_H
#define DB_H
#if ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#include <EEPROM.h>
#include "param.h"
#include "sensor.h"
#include "control.h"
#include "string.h"
#define K05 					512
#define K4  					4096
#define COUNT_ITEMS_FOR_05K 	20
#define COUNT_ITEMS_FOR_4K  	100
#define CHECK_BYTE 				0
#define BYTES_PER_RECORD 		20
#define DATA_WROTE_CORRECT_MARK 127
#define START_DATA_WROTE_MARK 	113
#define RECORD_EXIST 			100
#define RECORD_EMPTY 			0
#define SUCCESS_LOAD 			170
#define FAIL_LOAD 				0
#define START_BYTE_FOR_RECORDS 	150
#define ALL 					0
#define PARAM_TYPE 				1
#define SENSOR_TYPE 			2
#define CONTROL_TYPE 			3
#define FULL					4
#define PARAM_HASH_BYTE 		1
#define SENSOR_HASH_BYTE 		3
#define CONTROL_HASH_BYTE 		5
#define START_SSID_BYTE			7
#define END_SSID_BYTE			26
#define START_PASS_BYTE 		27
#define END_PASS_BYTE 			46
#define START_HOST_BYTE 		47
#define END_HOST_BYTE 			76
#define SUCCESS_LOAD_BYTE 		77

#define NONE 					0
#define SSID 					1
#define PASS 					2
#define HOST 					3
#define TIME 					4

class DB {
	public:
		DB(short size);
		int 	getFreeMemory();
		boolean isDataWroteCorrect();
		void 	addParam(unsigned short id, unsigned short roomId, uint8_t type, float value);
		void 	addSensor(unsigned short id, unsigned short paramId, uint8_t type, uint8_t port);
		void 	addControl(unsigned short id, unsigned short paramId, uint8_t type, uint8_t port, uint8_t timerEnable, uint32_t timerStart, uint32_t timerEnd, uint8_t timerDelay);
		void	addParamsFromString(const char * str);
		void	addSensorsFromString(const char * str);
		void	addControlsFromString(const char * str);
		void 	updateControl(unsigned short id, Control * control);
		void	writeParam(short i, unsigned short id, unsigned short roomId, uint8_t type, float value);
		void	writeSensor(short i, unsigned short id, unsigned short paramId, uint8_t type, uint8_t port);
		void	writeControl(short i, unsigned short id, unsigned short paramId, uint8_t type, uint8_t port, uint8_t timerEnable, uint32_t timerStart, uint32_t timerEnd, uint8_t timerDelay);
		void 	deleteParam(unsigned short id);
		void 	deleteSensor(unsigned short id);
		void 	deleteControl(unsigned short id);
		short	getControlByte(short i);
		short	getTypeByte(short i);
		uint8_t getParamsCount();
		uint8_t getSensorsCount();
		uint8_t getControlsCount();
		void 	*getRecords(uint8_t recordType);
		void	readParam(short i, Param *param);
		void	readSensor(short i, Sensor *sensor);
		void	readControl(short i, Control *control);
		Param 	*getParams();
		Sensor 	*getSensors();
		Control *getControls();
		void 	printParams(Param *params, uint8_t count);
		void 	printSensors(Sensor *sensors, uint8_t count);
		void 	printControls(Control *controls, uint8_t count);
		void	erase(uint8_t recordType = 0);
		void	addHash(unsigned short byteNumber, unsigned short hash);
		unsigned short getHash(unsigned short byteNumber);
		void 	writeString(char * str, unsigned short start, unsigned short end);
		void 	getString(char ** str, unsigned short start, unsigned short end);

	private:
		short 	_eepromSize;
		short 	_countOfItems;
		uint8_t _self_write_control = false;
		void 	startWriting();
		void 	endWriting();
		uint8_t getRecordsCount(uint8_t recordType);
		void 	deleteRecord(uint8_t index, uint8_t recordType);
};
#endif