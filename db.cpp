/*
 * Модуль для работы с базой данных контроллера
 * Алтухов Илья
 */

#include "DB.h"
extern int __bss_end;
extern void *__brkval;

// Возвращает количество свободной памяти
int DB::getFreeMemory() {
	int freeValue;

	if((int)__brkval == 0) {
		freeValue = ((int)&freeValue) - ((int)&__bss_end);
	} else {
		freeValue = ((int)&freeValue) - ((int)__brkval);
	}

	return freeValue;
}

// Функция-конструктор
// Аргументы:
// size - размер флеш-памяти контроллера
DB::DB(short size) {
	_eepromSize = size;
	switch (size) {
		case K05: _countOfItems = COUNT_ITEMS_FOR_05K;
		break;
		case K4: _countOfItems = COUNT_ITEMS_FOR_4K;
		break;
		default: _countOfItems = COUNT_ITEMS_FOR_05K;
	}
}

// Проверяет корректно ли записаны данные
boolean DB::isDataWroteCorrect() {
	byte status = 0;
	status = EEPROM.read(CHECK_BYTE);
	return status == DATA_WROTE_CORRECT_MARK;
}

// Ставит метку, что начата запись в память
void DB::startWriting(){
	EEPROM.update(CHECK_BYTE, START_DATA_WROTE_MARK);
}

// Убирает метку, что начата запись в память
void DB::endWriting(){
	EEPROM.update(CHECK_BYTE, DATA_WROTE_CORRECT_MARK);
}

// Добавляет параметр в базу
// Аргументы:
// id 	  - идентификатор параметра
// type   - тип параметра
// value  - целевое значение параметра
void DB::addParam(unsigned short id, unsigned short roomId, uint8_t type, float value) {
	short i, controlByte;
	for (i = START_BYTE_FOR_RECORDS; i < _eepromSize; i += BYTES_PER_RECORD) {
		controlByte = getControlByte(i);
		if (EEPROM.read(controlByte) != RECORD_EXIST) {
			writeParam(i, id, roomId, type, value);
			break;
		}
	}
}

// Добавляет датчик в базу
// Аргументы:
// id 	  - идентификатор датчика
// paramId - идентификатор параметра
// type   - тип датчика
// port   - порт датчика
void DB::addSensor(unsigned short id, unsigned short paramId, uint8_t type, uint8_t port) {
	short i, controlByte;
	for (i = START_BYTE_FOR_RECORDS; i < _eepromSize; i += BYTES_PER_RECORD) {
		controlByte = getControlByte(i);
		if (EEPROM.read(controlByte) != RECORD_EXIST) {
			writeSensor(i, id, paramId, type, port);
			break;
		}
	}
}

// Добавляет контрол в базу
// Аргументы:
// id - Идентификатор контрола
// paramId - идентификатор параметра
// type - тип контрола
// port - порт контрола
void DB::addControl(unsigned short id, unsigned short paramId, uint8_t type, uint8_t port, uint8_t timerEnable, uint32_t timerStart, uint32_t timerEnd, uint8_t timerDelay) {
	short i, controlByte;

	for (i = START_BYTE_FOR_RECORDS; i < _eepromSize; i += BYTES_PER_RECORD) {
		controlByte = getControlByte(i);
		if (EEPROM.read(controlByte) != RECORD_EXIST) {
			writeControl(i, id, paramId, type, port, timerEnable, timerStart, timerEnd, timerDelay);
			break;
		}
	}
}

// Добавляет параметры в базу
// Аргументы:
// str - сериализованные параметры с сервера
void DB::addParamsFromString(const char * str) {
	size_t len = slen(str);
	unsigned short id;
	unsigned short roomId;
	uint8_t type;
	float value;
	uint8_t argNumber = 0;
	size_t i;
	size_t j;
	size_t paramLen;
	char * paramBuf = NULL;
	char * argBuf = NULL;

	startWriting();
	_self_write_control = true;
	erase(PARAM_TYPE);

	if (len == 0) {
		return;
	}

	for (i = 0; i < len; i++) {
		if (str[i] == ':' || i == len - 1) {
			if (i == len - 1) {
				paramBuf = scat(paramBuf, str[i]);
			}
			paramLen = slen(paramBuf);
			for (j = 0; j < paramLen; j++) {
				if (paramBuf[j] == ',' || j == paramLen - 1) {
					if (j == paramLen - 1) {
						argBuf = scat(argBuf, paramBuf[j]);
					}
					switch (argNumber) {
						case 0:
							id = toUShort(argBuf);
						break;
						case 1:
							roomId = toUShort(argBuf);
						break;
						case 2:
							type = (uint8_t) toUShort(argBuf);
						break;
						case 3:
							value = toFloat(argBuf);
						break;
					}
					argNumber++;
					sclear(&argBuf);
				} else {
					argBuf = scat(argBuf, paramBuf[j]);
				}
			}
			sclear(&paramBuf);
			addParam(id, roomId, type, value);
			argNumber = 0;
		} else {
			paramBuf = scat(paramBuf, str[i]);
		}
	}

	_self_write_control = false;
	endWriting();
}

// Добавляет датчики в базу
// Аргуметы:
// str - сериализованные датчики с сервера
void DB::addSensorsFromString(const char * str) {
	size_t len = slen(str);
	unsigned short id;
	unsigned short paramId;
	uint8_t type;
	uint8_t port;
	uint8_t argNumber = 0;
	size_t i;
	size_t j;
	size_t sensorLen;
	char * sensorBuf = NULL;
	char * argBuf = NULL;

	startWriting();
	_self_write_control = true;
	erase(SENSOR_TYPE);

	if (len == 0) {
		return;
	}

	for (i = 0; i < len; i++) {
		if (str[i] == ':' || i == len - 1) {
			if (i == len - 1) {
				sensorBuf = scat(sensorBuf, str[i]);
			}
			sensorLen = slen(sensorBuf);
			for (j = 0; j < sensorLen; j++) {
				if (sensorBuf[j] == ',' || j == sensorLen - 1) {
					if (j == sensorLen - 1) {
						argBuf = scat(argBuf, sensorBuf[j]);
					}
					switch (argNumber) {
						case 0:
							id = toUShort(argBuf);
						break;
						case 1:
							paramId = toUShort(argBuf);
						break;
						case 2:
							type = (uint8_t) toUShort(argBuf);
						break;
						case 3:
							port = (uint8_t) toUShort(argBuf);
						break;
					}
					argNumber++;
					sclear(&argBuf);
				} else {
					argBuf = scat(argBuf, sensorBuf[j]);
				}
			}
			sclear(&sensorBuf);
			addSensor(id, paramId, type, port);
			argNumber = 0;
		} else {
			sensorBuf = scat(sensorBuf, str[i]);
		}
	}

	_self_write_control = false;
	endWriting();
}

// Добавляет контролы в базу
// Аргументы:
// str - сериализованные контролы с сервера
void DB::addControlsFromString(const char * str) {
	size_t len = strlen(str);
	unsigned short id;
	unsigned short paramId;
	uint8_t type;
	uint8_t port;
	uint8_t timerEnable;
	uint32_t timerStart;
	uint32_t timerEnd;
	uint8_t timerDelay;
	uint8_t argNumber = 0;
	size_t i;
	size_t j;
	uint8_t controlLen;
	char * controlBuf = NULL;
	char * argBuf = NULL;


	startWriting();
	_self_write_control = true;
	erase(CONTROL_TYPE);

	if (len == 0) {
		return;
	}

	for (i = 0; i < len; i++) {
		if (str[i] == ':' || i == len - 1) {
			if (i == len - 1) {
				controlBuf = scat(controlBuf, str[i]);
			}
			controlLen = slen(controlBuf);
			for (j = 0; j < controlLen; j++) {
				if (controlBuf[j] == ',' || j == controlLen - 1) {
					if (j == controlLen - 1) {
						argBuf = scat(argBuf, controlBuf[j]);
					}
					switch (argNumber) {
						case 0:
							id = toUShort(argBuf);
						break;
						case 1:
							paramId = toUShort(argBuf);
						break;
						case 2:
							type = (uint8_t) toUShort(argBuf);
						break;
						case 3:
							port = (uint8_t) toUShort(argBuf);
						break;
						case 4:
							timerEnable = (uint8_t) toUShort(argBuf);
						break;
						case 5:
							timerStart = (uint32_t) toULong(argBuf);
						break;
						case 6:
							timerEnd = (uint32_t) toULong(argBuf);
						break;
						case 7:
							timerDelay = (uint8_t) toUShort(argBuf);
						break;
					}
					argNumber++;
					sclear(&argBuf);
				} else {
					argBuf = scat(argBuf, controlBuf[j]);
				}
			}
			sclear(&controlBuf);
			addControl(id, paramId, type, port, timerEnable, timerStart, timerEnd, timerDelay);
			argNumber = 0;
		} else {
			controlBuf = scat(controlBuf, str[i]);
		}
	}

	_self_write_control = false;
	endWriting();
}

void DB::updateControl(unsigned short id, Control * control) {
	short i;
	unsigned short 	currentId;
	unsigned short 	paramId 	= control->getParamId();
	uint8_t 		type 		= control->getControlType();
	uint8_t 		port 		= control->getPort();
	uint8_t 		timerEnable = control->isTimerEnable();
	uint32_t 		timerStart 	= control->getTimerStart();
	uint32_t 		timerEnd 	= control->getTimerEnd();
	uint8_t 		timerDelay 	= control->isTimerDelay();
	short 			controlByte;
	short 			typeByte;

	for (i = START_BYTE_FOR_RECORDS; i < _eepromSize; i += BYTES_PER_RECORD) {
		controlByte = getControlByte(i);
		typeByte 	= getTypeByte(i);
		EEPROM.get(i, currentId);

		if (currentId != id) 							continue;
		if (EEPROM.read(controlByte) != RECORD_EXIST) 	continue;
		if (EEPROM.read(typeByte) != CONTROL_TYPE) 		continue;

		writeControl(i, id, paramId, type, port, timerEnable, timerStart, timerEnd, timerDelay);
		break;
	}
}

// Записывает параметр во флеш-память контроллера
// Аргументы:
// i 	  - стартовый номер ячейки памяти
// id 	  - идентификатор параметра
// type   - тип параметра
// value  - целевое значение параметра
void DB::writeParam(short i, unsigned short id, unsigned short roomId, uint8_t type, float value) {
	short seek, controlByte, typeByte;
	seek = i;
	controlByte = getControlByte(i);
	typeByte = getTypeByte(i);
	if (!_self_write_control) {
		startWriting();
	}
	EEPROM.put(seek, id);
	seek += 2;
	EEPROM.put(seek, roomId);
	seek += 2;
	EEPROM.put(seek, type);
	seek++;
	EEPROM.put(seek, value);
	EEPROM.update(controlByte, RECORD_EXIST);
	EEPROM.update(typeByte, PARAM_TYPE);
	if (!_self_write_control) {
		endWriting();
	}
}

// Записывает датчик во флеш-память контроллера
// Аргументы:
// i 	  - стартовый номер ячейки памяти
// id 	  - идентификатор датчика
// paramId - идентификатор параметра
// type   - тип датчика
// port   - порт датчика
void DB::writeSensor(short i, unsigned short id, unsigned short paramId, uint8_t type, uint8_t port) {
	short seek, controlByte, typeByte;
	seek = i;
	controlByte = getControlByte(i);
	typeByte = getTypeByte(i);
	if (!_self_write_control) {
		startWriting();
	}
	EEPROM.put(seek, id);
	seek += 2;
	EEPROM.put(seek, paramId);
	seek += 2;
	EEPROM.put(seek, type);
	seek++;
	EEPROM.put(seek, port);
	EEPROM.update(controlByte, RECORD_EXIST);
	EEPROM.update(typeByte, SENSOR_TYPE);
	if (!_self_write_control) {
		endWriting();
	}
}


// Записывает контрол во флеш-память контроллера
// i 	  - стартовый номер ячейки памяти
// id 	  - идентификатор контрола
// paramId - идентификатор параметра
// type   - тип контрола
// port   - порт контрола
void DB::writeControl(short i, unsigned short id, unsigned short paramId, uint8_t type, uint8_t port, uint8_t timerEnable, uint32_t timerStart, uint32_t timerEnd, uint8_t timerDelay) {
	short seek, controlByte, typeByte;
	seek = i;
	controlByte = getControlByte(i);
	typeByte = getTypeByte(i);
	if (!_self_write_control) {
		startWriting();
	}
	EEPROM.put(seek, id);
	seek += 2;
	EEPROM.put(seek, paramId);
	seek +=2;
	EEPROM.put(seek, type);
	seek++;
	EEPROM.put(seek, port);
	seek++;
	EEPROM.put(seek, timerEnable);
	seek++;
	EEPROM.put(seek, timerStart);
	seek += 4;
	EEPROM.put(seek, timerEnd);
	seek += 4;
	EEPROM.put(seek, timerDelay);
	EEPROM.update(typeByte, CONTROL_TYPE);
	EEPROM.update(controlByte, RECORD_EXIST);
	if (!_self_write_control) {
		endWriting();
	}
}

// Возвращает номер контрольного байта записи
// Аргументы:
// i - стартовый номер ячейки памяти
short DB::getControlByte(short i) {
	return i + BYTES_PER_RECORD - 2;
}

// Возвращает номер байта, который отвечает за тип записи
// Аргументы:
// i - стартовый номер ячейки памяти
short DB::getTypeByte(short i) {
	return i + BYTES_PER_RECORD - 1;
}

// Удаляет параметр из базы
// Аргументы:
// id - идентификатор параметра
void DB::deleteParam(unsigned short id) {
	deleteRecord(id, PARAM_TYPE);
}

// Удаляет датчик из базы
// Аргументы:
// id - идентификатор датчика
void DB::deleteSensor(unsigned short id) {
	deleteRecord(id, SENSOR_TYPE);
}

// Удаляет контролы из базы
// Аргументы:
// id - идентификатор контрола
void DB::deleteControl(unsigned short id) {
	deleteRecord(id, CONTROL_TYPE);
}

// Возвращает количество параметров в базе
uint8_t DB::getParamsCount() {
	return getRecordsCount(PARAM_TYPE);
}

// Возвращает количество датчиков в базе
uint8_t DB::getSensorsCount() {
	return getRecordsCount(SENSOR_TYPE);
}

// Возвращает количество контролов в базе
uint8_t DB::getControlsCount() {
	return getRecordsCount(CONTROL_TYPE);
}

// Возвращает количество записей в базе
// Аргументы:
// recordType - тип записи
uint8_t DB::getRecordsCount(uint8_t recordType) {
	uint8_t count = 0;
	//uint8_t lastTimeSuccessLoaded = EEPROM.read(SUCCESS_LOAD_BYTE);
	short i, controlByte, typeByte;

	/*if (lastTimeSuccessLoaded == FAIL_LOAD) {
		return 0;
	}*/

	for (i = START_BYTE_FOR_RECORDS; i < _eepromSize; i+= BYTES_PER_RECORD) {
		controlByte = getControlByte(i);
		typeByte = getTypeByte(i);

		if (EEPROM.read(controlByte) == RECORD_EXIST && EEPROM.read(typeByte) == recordType) {
			count++;
		}
	}

	return count;
}

// Удаляет запись из базы
// Аргументы:
// recordId   - идентификатор записи
// recordType - тип записи
void DB::deleteRecord(uint8_t recordId, uint8_t recordType) {
	short i, controlByte, typeByte, seek;
	unsigned short id, type;
	for (i = START_BYTE_FOR_RECORDS; i < _eepromSize; i+= BYTES_PER_RECORD) {
		controlByte = getControlByte(i);
		typeByte = getTypeByte(i);
		seek = i;
		EEPROM.get(id, seek);
		seek += 2;
		EEPROM.get(type, seek);
		if (id == recordId && type == recordType) {
			startWriting();
			EEPROM.update(controlByte, RECORD_EMPTY);
			endWriting();
			break;
		}
	}
}

// Возвращает массив параметров
Param *DB::getParams() {
	return (Param *) getRecords(PARAM_TYPE);
}

// Возвращает массив датчиков
Sensor *DB::getSensors() {
	return (Sensor *) getRecords(SENSOR_TYPE);
}

// Возвращает массив контролов
Control *DB::getControls() {
	return (Control*) getRecords(CONTROL_TYPE);
}

// Возвращает массив записей
// Аргументы
// recordType - тип записи
void *DB::getRecords(uint8_t recordType) {
	uint8_t recordsCount = 0;
	uint8_t recordsRecieved = 0;
	short i, controlByte, typeByte, j;
	Param *params = NULL;
	Sensor *sensors = NULL;
	Control *controls = NULL;
	void *records = NULL;
	//uint8_t lastTimeSuccessLoaded = EEPROM.read(SUCCESS_LOAD_BYTE);

	/*if (lastTimeSuccessLoaded == FAIL_LOAD) {
		return NULL;
	}*/

	switch (recordType) {
		case PARAM_TYPE:
			recordsCount = getParamsCount();
			//EEPROM.write(SUCCESS_LOAD_BYTE, FAIL_LOAD);
			params = (Param *) malloc(sizeof(Param) * recordsCount);
		break;
		case SENSOR_TYPE:
			recordsCount = getSensorsCount();
			//EEPROM.write(SUCCESS_LOAD_BYTE, FAIL_LOAD);
			sensors = (Sensor *) malloc(sizeof(Sensor) * recordsCount);
		break;
		case CONTROL_TYPE:
			recordsCount = getControlsCount();
			//EEPROM.write(SUCCESS_LOAD_BYTE, FAIL_LOAD);
			controls = (Control *) malloc(sizeof(Control) * recordsCount);
		break;
	}

	for (i = START_BYTE_FOR_RECORDS; i < _eepromSize; i += BYTES_PER_RECORD) {
		controlByte = getControlByte(i);
		typeByte = getTypeByte(i);
		if (EEPROM.read(controlByte) == RECORD_EXIST && EEPROM.read(typeByte) == recordType) {
			switch (recordType) {
				case PARAM_TYPE:
					readParam(i, &params[recordsRecieved]);
				break;
				case SENSOR_TYPE:
					readSensor(i, &sensors[recordsRecieved]);
				break;
				case CONTROL_TYPE:
					readControl(i, &controls[recordsRecieved]);
				break;
			}
			recordsRecieved++;
		}
	}


	if (recordType == SENSOR_TYPE) {
		for (i = 0; i < recordsRecieved; i++) {
			for (j = 0; j <recordsRecieved; j++) {
				if (sensors[i].getSensorType() != sensors[j].getSensorType()) {
					continue;
				}

				if (sensors[i].getSensorPort() != sensors[j].getSensorPort()) {
					continue;
				}

				if (sensors[i].getSensorId() == sensors[j].getSensorId()) {
					continue;
				}

				if (sensors[i].alias != NULL) {
					continue;
				}

				sensors[j].alias = &sensors[i];
			}
		}
	}

	switch (recordType) {
		case PARAM_TYPE:
			records = (void *) params;
		break;
		case SENSOR_TYPE:
			records = (void *) sensors;
		break;
		case CONTROL_TYPE:
			records = (void *) controls;
		break;
	}

	//EEPROM.write(SUCCESS_LOAD_BYTE, SUCCESS_LOAD);

	return records;
}

// Читает параметр из флеш-памяти контроллера
// Аргументы:
// i 	 - стартовый номер ячейки памяти
// param - указатель на переменную в которую будет записан параметр
void DB::readParam(short i, Param *param) {
	short seek = i;
	unsigned short id;
	unsigned short roomId;
	uint8_t type;
	float value;
	EEPROM.get(seek, id);
	seek += 2;
	EEPROM.get(seek, roomId);
	seek += 2;
	EEPROM.get(seek, type);
	seek++;
	EEPROM.get(seek, value);
	*param = Param(id, roomId, type, value);
}

// Читает датчик из памяти флеш-контроллера
// Аргументы:
// i 	  - стартовый номер ячейки памяти
// sensor - указатель на переменную в которую будет записан датчик
void DB::readSensor(short i, Sensor *sensor) {
	short seek = i;
	unsigned short id;
	unsigned short paramId;
	uint8_t type;
	uint8_t port;
	EEPROM.get(seek, id);
	seek += 2;
	EEPROM.get(seek, paramId);
	seek += 2;
	EEPROM.get(seek, type);
	seek++;
	EEPROM.get(seek, port);
	*sensor = Sensor(id, paramId, type, port);
}

// Читает контрол из памяти флеш-контроллера
// Аргументы:
// i 	   - стартовый номер ячейки памяти
// control - указатель на переменню в которую будет записан контрол
void DB::readControl(short i, Control *control) {
	short seek = i;
	unsigned short id;
	unsigned short paramId;
	uint8_t type;
	uint8_t port;
	uint8_t timerEnable;
	uint32_t timerStart;
	uint32_t timerEnd;
	uint8_t timerDelay;
	EEPROM.get(seek, id);
	seek += 2;
	EEPROM.get(seek, paramId);
	seek += 2;
	EEPROM.get(seek, type);
	seek++;
	EEPROM.get(seek, port);
	seek++;
	EEPROM.get(seek, timerEnable);
	seek++;
	EEPROM.get(seek, timerStart);
	seek += 4;
	EEPROM.get(seek, timerEnd);
	seek += 4;
	EEPROM.get(seek, timerDelay);
	*control = Control(id, paramId, type, port, timerEnable, timerStart, timerEnd, timerDelay);
}

// Выводит список параметров в терминал
// Аргументы:
// params - массив с параметрами
void DB::printParams(Param *params, uint8_t count) {
	uint8_t i;
	Param *param;
	Serial.println();
	Serial.println(F("========================================"));
	Serial.println();

	if (count == 0) {
		Serial.println(F("No params"));
		Serial.println();
	} else {
		Serial.print(F("Params count: "));
		Serial.println(count);

		for (i = 0; i < count; i++) {
			param = &params[i];
			Serial.print(F("Param ID: "));
			Serial.println(param->getParamId());
			Serial.print(F("Room id: "));
			Serial.println(param->getRoomId());
			Serial.print(F("Param type: "));
			Serial.println(param->getParamType());
			Serial.print(F("Value: "));
			Serial.println(param->getTargetValue());
			Serial.println();
		}
	}

	Serial.println(F("========================================"));
	Serial.println();
}

// Выводит список датчиков в терминал
// Аргументы:
// sensors - массив с датчиками
void DB::printSensors(Sensor *sensors, uint8_t count) {
	uint8_t i;
	Sensor *sensor;
	Serial.println();
	Serial.println(F("========================================"));
	Serial.println();

	if (count == 0) {
		Serial.println(F("No sensors"));
		Serial.println();
	} else {
		Serial.print(F("Sensors count: "));
		Serial.println(count);

		for (i = 0; i < count; i++) {
			sensor = &sensors[i];
			Serial.print(F("Sensor ID: "));
			Serial.println(sensor->getSensorId());
			Serial.print(F("Sensor type: "));
			Serial.println(sensor->getSensorType());
			Serial.print(F("Param ID: "));
			Serial.println(sensor->getParamId());
			Serial.print(F("Sensor port: "));
			Serial.println(sensor->getSensorPort());
			Serial.println();
		}
	}

	Serial.println(F("========================================"));
	Serial.println();
}

// Выводит список контролов в терминал
// Аргументы:
// controls - массив с контролами
void DB::printControls(Control *controls, uint8_t count) {
	uint8_t i;
	Control *control;
	Serial.println();
	Serial.println(F("========================================"));
	Serial.println();

	if (count == 0) {
		Serial.println(F("No controls"));
		Serial.println();
	} else {
		Serial.print(F("Controls count: "));
		Serial.println(count);

		for (i = 0; i < count; i++) {
			control = &controls[i];
			Serial.print(F("Control ID: "));
			Serial.println(control->getControlId());
			Serial.print(F("Control type: "));
			Serial.println(control->getControlType());
			Serial.print(F("Param ID: "));
			Serial.println(control->getParamId());
			Serial.print(F("Control port: "));
			Serial.println(control->getPort());
			Serial.print(F("Timer enable: "));
			Serial.println(control->isTimerEnable());
			Serial.print(F("Timer start: "));
			Serial.println(control->getTimerStart());
			Serial.print(F("Timer end: "));
			Serial.println(control->getTimerEnd());
			Serial.print(F("Timer delay: "));
			Serial.println(control->isTimerDelay());
			Serial.println();
		}
	}

	Serial.println(F("========================================"));
	Serial.println();
}

// Удаляет записи определенного типа
// Аргументы:
// recordType - тип записей
void DB::erase(uint8_t recordType) {
	short i, controlByte, typeByte;
	uint8_t eraseProgress;

	for (i = START_BYTE_FOR_RECORDS; i < _eepromSize - BYTES_PER_RECORD; i += BYTES_PER_RECORD) {
		controlByte = getControlByte(i);
		
		if (recordType == ALL) {
			EEPROM.update(controlByte, RECORD_EMPTY);
		} else {
			typeByte = getTypeByte(i);
			if (EEPROM.read(controlByte) == RECORD_EXIST && EEPROM.read(typeByte) == recordType) {
				EEPROM.update(controlByte, RECORD_EMPTY);
			}
		}
	}

	switch (recordType) {
		case PARAM_TYPE:
			addHash(PARAM_HASH_BYTE, 0);
		break;
		case SENSOR_TYPE:
			addHash(SENSOR_HASH_BYTE, 0);
		break;
		case CONTROL_TYPE:
			addHash(CONTROL_HASH_BYTE, 0);
		break;
		case FULL:
			for (i = START_BYTE_FOR_RECORDS; i < _eepromSize; i++) {
				EEPROM.write(i, 0);

				if (eraseProgress == map(i, START_BYTE_FOR_RECORDS, _eepromSize - 1, 0, 100)) {
					continue;
				} else {
					eraseProgress = map(i, START_BYTE_FOR_RECORDS, _eepromSize - 1, 0, 100);
				}

				if (eraseProgress % 10 == 0) {
					Serial.print(eraseProgress);
					Serial.println("%");
				}
			}
		case ALL:
			addHash(PARAM_HASH_BYTE, 0);
			addHash(SENSOR_HASH_BYTE, 0);
			addHash(CONTROL_HASH_BYTE, 0);
		break;
	}
}

// Добавляет контрольную сумму в базу
// Аргументы:
// byteNumber - номер байта начиная с которого будет записана контрольная сумма
// hash - записываемя контрольная сумма
void DB::addHash(unsigned short byteNumber, unsigned short hash) {
	EEPROM.put(byteNumber, hash);
}

// Возвращает контрольную сумму из базы контроллера
// Аргументы:
// byteNumber - номер байта с которого начинается чтение контрольной суммы
unsigned short DB::getHash(unsigned short byteNumber) {
	unsigned short hash = 0;
	//uint8_t lastTimeSuccessLoaded = EEPROM.read(SUCCESS_LOAD_BYTE);

	/*if (lastTimeSuccessLoaded == FAIL_LOAD) {
		return 0;
	}*/

	EEPROM.get(byteNumber, hash);

	return hash;
}

// Записывает строку в память
// Аргументы:
// str - строка, которая будет записана в память
// start - номер начального байта
// end - номер конечного байта
void DB::writeString(char * str, unsigned short start, unsigned short end) {
	unsigned short neededSymbols = slen(str) - 1;
	unsigned short i;
	unsigned short symbols = 0;

	for (i = start; i <= end; i++, symbols++) {
		EEPROM.write(i, str[symbols]);
		if (symbols == neededSymbols) break;
	}

	if (++i < end) {
		EEPROM.write(i, 0);
	}
}

// Записывает строку в память
// Аргументы:
// str - указатель, в который будет записана считанная информация
// start - номер начального байта
// end - номер конечного байта
void DB::getString(char ** str, unsigned short start, unsigned short end) {
	unsigned short i;
	char ch;

	for (i = start; i <= end; i++) {
		ch = EEPROM.read(i);

		if (ch == '\0') {
			break;
		}
		
		*str = scat(*str, ch);
	}
}