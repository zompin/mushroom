#include "diagnostic.h"

// Возвращает объем свободной памяти
int getFreeMemory() {
	int freeValue;

	if((int)__brkval == 0) {
		freeValue = ((int)&freeValue) - ((int)&__bss_end);
	} else {
		freeValue = ((int)&freeValue) - ((int)__brkval);
	}

	return freeValue;
}