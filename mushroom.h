/*
 * Прошивка для системы управления климатом
 * Алтухов Илья
 */

#include "param.h"
#include "sensor.h"
#include "control.h"
#include "DB.h"
#include "network.h"
#include "std.h"
#include "DS3231.h"
#define FIRMWARE_VERSION	"Firmware version: 1.5.13-alpha 5.04.2017"
#define PARAM_HASH 			0
#define SENSOR_HASH 		1
#define CONTROL_HASH 		2
#define HASHES_REQUEST		1
#define GET_CONFIG_REQUEST 	2
#define SEND_DATA_REQUEST 	3
#define PROD 				1
#define TEST				2

struct _ParamBackup {
	unsigned short id;
	boolean isOnLowControlsWork;
	boolean isOnHighControlsWork;
};

struct _ControlBackup {
	uint8_t port;
	uint8_t type;
	unsigned short paramId;
};

typedef struct _ParamBackup ParamBackup;
typedef struct _ControlBackup ControlBackup;

void 				getData();
void 				sendData();
char * 				serializeParams();
char * 				serializeSensors();
char * 				serializeControls();
boolean 			isStringCorrect(const char * str);
void 				lookForChanges(const unsigned short * str);
void 				updateRecords(const unsigned short * hashes, uint8_t hashType);
void 				updateParams(char * data, unsigned short hash);
void 				updateSensors(char * data, unsigned short hash);
void 				updateControls(char * data, unsigned short hash);
unsigned short * 	explodeHashes(const char * str);
char * 				getDataFromString(const char * str);
//char * 				getDataFromString(char * str);
unsigned short 		getHashFromString(const char * str);
void 				lookForParametr(uint8_t paramType);
float 				getCurrentParamValue(Param * param, float tmp = NAN, float hum = NAN);
void 				controlRullingByTime();
void 				onLowControlRuling(Param * param);
void 				onHighControlRuling(Param * param);
void 				controlsTurnOn(Param * param, uint8_t controlType);
void 				controlsTurnOff(Param * param, uint8_t controlType);
Param * 			getParamByTypeAndRoomId(uint8_t type, unsigned short roomId);
void				command();
void				backupParams();
void				backupControls();
void				alignParams();
void				alignControls();