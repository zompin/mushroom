/*
 * Библиотке для работы с NTC-термистором
 */

#include "NTC.h"
#define COUNTOFSAMPLES 25
#define TEMPERATURENOMINAL 25

// Функция конструктор
// Аргументы:
// port - порт термистора
NTC::NTC(byte port) {
	_port = port;
}

// Возвращает значение температуры датчика
float NTC::getTemperature() {
	byte i;
	float average = 0;
	float steinhart;
	int samples[COUNTOFSAMPLES];

	for (i=0; i< COUNTOFSAMPLES; i++) {
		samples[i] = analogRead(_port);
		average += samples[i];
	}

	average /= COUNTOFSAMPLES;
	if ((short)average <= 1 || (short)average >= 1022) {
		steinhart = NAN;
	} else {
		average = 1023 / average - 1;
		average = _resistance / average;
		steinhart = average / _termistorResistance;
		steinhart = log(steinhart);
		steinhart /= _bcoefficient;
		steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15);
		steinhart = 1.0 / steinhart;
		steinhart -= 273.15;
		steinhart *= 10;
		steinhart = (float)round(steinhart) / 10;
	}
	return steinhart;
}

// Устанавливает значение сопротивление термистора при нормальных условиях
void NTC::setTermistorResistance(long termistorResistance) {
	_termistorResistance = termistorResistance;
	return;
}

// Устанавливает значение сопротивление резистора
void NTC::setResistance(long resistance) {
	_resistance = resistance;
	return;
}

// Устанавливает значение температурного коэффициента
void NTC::setBCoefficient(byte bcoefficient) {
	_bcoefficient = bcoefficient;
	return;
}