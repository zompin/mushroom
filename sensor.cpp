/*
 * Библиотека для работы с датчиками
 * Алтухов Илья
 */

#include "sensor.h"

// Функция-конструктор
// Аргументы:
// id 		- идентификатор датчика в базе
// paramId 	- идентификатор параметра в базе
// type 	- тип датчика
// port 	- порт датчика
Sensor::Sensor(
	unsigned short id, 
	unsigned short paramId, 
	uint8_t type, 
	uint8_t port
) : 
_dht(port, type), 
_mq135(port), 
_ntc(port)/*, 
_wire(port), 
_ds18b20()*/ {
	_id 		= id;
	_paramId 	= paramId;
	_type 		= type;
	_port 		= port;
	
	pinMode(_port, INPUT);
	DHT_init();
	//ds18b20_init(&_wire);
	clearUselessData();
}

// Возвращает идентификатор датчика
unsigned short Sensor::getSensorId() {
	return _id;
}

// Возвращает идентификатор параметра
unsigned short Sensor::getParamId() {
	return _paramId;
}

// Возвращает тип датчика
uint8_t Sensor::getSensorType() {
	return _type;
}

// Возвращает порт датчика
uint8_t Sensor::getSensorPort() {
	return _port;
}

// Инициализирует датчики семейства DHT
void Sensor::DHT_init() {
	if (isDHT() && alias == NULL) {
		_dht.begin();
	}
}

// Инициализирует датчик ds18b20
/*void Sensor::ds18b20_init(OneWire * _wire) {
	if (_type == DS18B20) {
		_ds18b20.setOneWire(_wire);
		_ds18b20.begin();
	}
}*/

// Удаляет неиспользуемые экземпляры объектов датчиков
void Sensor::clearUselessData() {
	if (!isDHT()) {
		_dht.~DHT();
	}

	if (_type != MQ135_CO2) {
		_mq135.~MQ135();
	}

	if (_type != NTCS) {
		_ntc.~NTC();
	}

	/*if (_type != DS18B20) {
		_ds18b20.~DallasTemperature();
		_wire.~OneWire();
	}*/
}

// Возвращает значение влажности на датчике
float Sensor::getHumidity() {
	float humidity = 0;

	if (isDHT()) {
		if (alias == NULL){
			humidity = _dht.readHumidity();
		} else {
			humidity = alias->getHumidity();
		}
	}

	return humidity;
}

// Возвращает значение температуры на датчике
float Sensor::getTemperature() {
	float 	temperature = NAN;
	float 	buff 		= 0;
	uint8_t count;
	uint8_t i;

	if (isDHT()) {
		if (alias == NULL) {
			temperature = _dht.readTemperature();	
		} else {
			temperature = alias->getTemperature();
		}
	}

	if (_type == NTCS) {
		temperature = _ntc.getTemperature();
	}

	/*if (_type == DS18B20) {
		count = _ds18b20.getDeviceCount();
		_ds18b20.requestTemperatures();

		for (i = 0; i < count; i++) {
			buff + _ds18b20.getTempCByIndex(i);
		}

		temperature = buff / count;
	}*/

	return temperature;
}

// Отключает датчик, чтобы с него не читались показания
void Sensor::disableSensor() {
	_enable = false;
}

// Отключает датчик, чтобы с него нельзы было считывать данные
boolean Sensor::isEnable() {
	return _enable;
}

// Проверяет датчик на принадлежность семейству DHT
boolean Sensor::isDHT() {
	return _type == DHT11 || _type == DHT21 || _type == DHT22 || _type == AM2301;
}

// Возвращает значение CO2
float Sensor::getCO2Ppm() {
	return _mq135.getPPM();
}

// Возвращает скорректированное значение CO2 с учетом температуры и влажности
// Аргументы:
// t - температура
// h - влажность
float Sensor::getCorrectedCO2Ppm(float t, float h) {
	return _mq135.getCorrectedPPM(t, h);
}

// Обновляет значение датчика в зависимости от типа параметра
// Аргументы:
// paramType - тип параметра
void Sensor::updateValue(uint8_t paramType, float tmp, float hum) {

	switch (paramType) {
		case HUM:
			_value = getHumidity();
		break;
		case TMP:
			_value = getTemperature();
		break;
		case CO2:
			if (tmp == tmp && hum == hum) {
				_value = getCorrectedCO2Ppm(tmp, hum);
			} else {
				_value = getCO2Ppm();
			}
		break;
	}

}

// Возвращает значение датчика
float Sensor::getValue() {
	return _value;
}

// Устанавливает текущее значение, для тестовых нужд
void Sensor::setValue(float value) {
	_value = value;
}