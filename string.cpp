#include "string.h"

// Возвращает длину строки
size_t slen(char * str) {
	size_t len = 0;

	if (str != NULL) {
		while (str[len] != '\0') {
			len++;
		}
	}

	return len;
}


// Соединяет две строки
char * scat(char * str1, char * str2) {
	size_t len1 = slen(str1);
	size_t len2 = slen(str2);
	size_t i, j;

	str1 = (char *) realloc(str1, sizeof(char) * (len1 + len2 + 1));

	if (str1 != NULL) {
		for (i = len1, j = 0; j < len2; i++, j++) {
			str1[i] = str2[j];
		}

		str1[i] = '\0';
	}

	return str1;
}

// Присоединяет символ к строке
char * scat(char * str, char ch) {
	size_t len = slen(str);
	
	str = (char *) realloc(str, sizeof(char) * (len + 2));

	if (str != NULL) {
		str[len] = ch;
		str[++len] = '\0';
	}

	return str;
}

// Возвращает первое вхождение символа с начала строки
size_t indexOf(char * str, char ch) {
	size_t len = slen(str);
	size_t pos = -1;
	size_t i;

	if (str != NULL) {
		for (i = 0; i < len; i++) {
			if (str[i] == ch) {
				pos = i;
				break;
			}
		}
	}

	return pos;
}

// Возвращает первое вхождение символа с конца строки
size_t lasIndexOf(char * str, char ch) {
	size_t len = slen(str);
	size_t pos = -1;
	size_t i;

	if (str != NULL) {
		for (i = len - 1; i >= 0; i--) {
			if (str[i] == ch) {
				pos = i;
				break;
			}
		}
	}

	return pos;
}

// Сравнивает две строки
uint8_t scomp(char * str1, char * str2) {
	size_t len1 = slen(str1);
	size_t len2 = slen(str2);
	size_t i;

	if (len1 == len2) {
		for (i = 0; i < len1; i++) {
			if (str1[i] != str2[i]) {
				return 0;
			}
		}
	} else {
		return 0;
	}

	return 1;
}

// Возвращает копию строки
char * scopy(char * str) {
	size_t len 	= str;
	char * cp 	= NULL;
	size_t i;

	if (len > 0) {
		cp = (char *) malloc(sizeof(char) * (len + 1));

		if (cp != NULL) {
			for (i = 0; i < len; i++) {
				cp[i] = str[i];
			}

			cp[len] = '\0';
		}
	}

	return cp;
}

// Безопасно очищает строку
void sclear(char ** str) {
	free(*str);
	*str = NULL;
}

// Конвертирует строку в число, double
double toDouble(char * str) {
	return strtod(str, (char **) NULL);
}

// Конвертирует строку в число, float
float toFloat(char * str) {
	return (float) toDouble(str);
}

// Конвертирует строку в число, long
long toLong(char * str) {
	return strtol(str, (char **) NULL, 10);
}

// Конвертирует строку в число, unsigned long
unsigned long toULong(char * str) {
	return strtoul(str, (char **) NULL, 10);
}

unsigned short toUShort(char * str) {
	return (unsigned short) toULong(str);
}

// Конвертирует число (double) в строку
char * doubleToString(double val, signed char width, unsigned char prec) {
	char * s = NULL;
	s = (char *) malloc(sizeof(char) * width);

	if (s != NULL) {
		dtostrf(val, 1, prec, s);
	}

	return s;
}

// Конвертирует число (float) в строку
char * floatToString(float val, signed char width, unsigned char prec) {
	return doubleToString((double) val, width, prec);
}

// Конвертирует число (long) в строку
char * longToString(long val, signed char width) {
	char * s = NULL;
	s = (char *) malloc(sizeof(char) * width);

	if (s != NULL) {
		ltoa(val, s, 10);
	}

	return s;
}

// Конвертирует число (unsigned long) в строку
char * uLongToString(unsigned long val, signed char width) {
	char * s = NULL;
	s = (char *) malloc(sizeof(char) * width);

	if (s != NULL) {
		ultoa(val, s, 10);
	}

	return s;
}

// Конвертирует число (unsigned short) в строку
char * uShortToString(unsigned short val, signed char width) {
	return uLongToString((unsigned long) val, width);
}