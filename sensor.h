/*
 * Библиотека для работы с датчиками
 * Алтухов Илья
 */

#ifndef SENSOR_H
#define SENSOR_H
#include "DHT.h"
#include "MQ135.h"
#include "ntc.h"
//#include "OneWire.h"
//#include "DallasTemperature.h"
#include "param.h"
#define NTCS 		1
#define DS18B20 	2
#define MQ135_CO2 	3

#if ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

class Sensor {
	public:
		Sensor(unsigned short id, unsigned short paramId, uint8_t type, uint8_t port);
		unsigned short 	getSensorId();
		unsigned short 	getParamId();
		uint8_t 		getSensorType();
		uint8_t 		getSensorPort();
		void 			DHT_init();
		//void 			ds18b20_init(OneWire * _wire);
		void 			clearUselessData();
		float 			getHumidity();
		float 			getTemperature();
		void 			disableSensor();
		float 			getCO2Ppm();
		float 			getCorrectedCO2Ppm(float t, float h);
		boolean 		isEnable();
		void			updateValue(uint8_t paramType, float tmp = NAN, float hum = NAN);
		float			getValue();
		void 			setValue(float value);
		Sensor* 		alias 		= NULL;
		boolean			isAliased 	= false;

	private:
		boolean isDHT();
		unsigned short 	_id 		= 0;
		unsigned short 	_paramId 	= 0;
		uint8_t 		_type 		= 0;
		uint8_t 		_port 		= 0;
		float 			_value 		= 0;
		boolean 		_enable 	= true;
		DHT 			_dht;
		MQ135 			_mq135;
		NTC 			_ntc;
		//OneWire 		_wire;
		//DallasTemperature _ds18b20;
};

#endif