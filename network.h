/*
 * Модуль для работы с сетью
 * Алтухов Илья
 */

#ifndef NETWORK_H
#define NETWORK_H

#if ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif
#include "SoftwareSerial.h"
#include "std.h"
#define GET 1
#define POST 2
	struct _Request {
		unsigned long timer;
		uint8_t id;
	};
	typedef struct _Request Request;

class Esp{
	public:
		Esp(uint8_t en_pin);
		void	request(uint8_t id);
		void	enableLog();
		void	disableLog();
		void 	toggleLog();
		uint8_t	getStatus();
		unsigned short crc16(const char * str);
		char 	* ssid = NULL;
		char	* password = NULL;
		char	* host = NULL;
		char	* path = "/";
		char	* port = "80";
		uint8_t	* method = GET;
		char	* input = NULL;
		char	* output = NULL;
		boolean	close_connection_between_requests = true;
	private:
		void	espAction();
		void	espWait();
		Request *getTimer(uint8_t id);
		void	addRequest(uint8_t id);
		void	searchForEsp();
		void	connectToWiFi();
		void	connectToHost();
		void	requestToHost();
		void	closeConnection();
		void	waitForEsp(char * answer);
		void	waitForWiFi(char * answer);
		void	waitForHost(char * answer);
		void	waitForRequest(char * answer);
		void	waitForStartRecievingData(char * answer);
		void	recievingData(char * answer);
		void	waitForClosingConnection(char * answer);
		char	* getCacheOfString();
		Request *_requestsArray;
		uint8_t	_id;
		uint8_t _requestsCount = 0;
		uint8_t _en_pin = 0;
		char	* _query = NULL;
		char	* _buffer;
		boolean	_busy = false;
		boolean _cache = false;
		boolean _has_task = true;
		boolean _do_log = false;
		boolean _write_to_buffer = false;
		#define BUSY_TIMEOUT 15000
		#define ERROR_TIMEOUT 15000
		#define REQUEST_TIMEOUT 15000
		//#define ERROR_TIMEOUT 10000
		//#define REQUEST_TIMEOUT 10000
		#define ESP_NOT_FOUND 0
		#define ESP_FOUND 1
		#define WIFI_CONNECTED 2
		#define HOST_CONNECTED 3
		#define WAIT_FOR_REQUEST 4
		#define RECIEVING_DATA 5
		#define DATA_RECIEVED 6
		uint8_t _status = ESP_NOT_FOUND;
		uint8_t _prev_status = 0;
		unsigned long _busy_timer = 0;
		unsigned long _task_timer = 0;
		unsigned long _between_errors_timer = 0;
		unsigned long _between_request_timer = 0;
};

#endif