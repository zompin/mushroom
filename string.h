#include <inttypes.h>
#include <stdlib.h>

#ifndef __STRING__
#define __STRING__

size_t 			slen(char * str);
char * 			scat(char * str1, char * str2);
char * 			scat(char * str1, char ch);
size_t 			indexOf(char * str, char ch);
size_t 			lastIndexOf(char * str, char ch);
uint8_t 		scomp(char * str1, char * str2);
char * 			scopy(char * str);
void 			sclear(char ** str);
double 			toDouble(char * str);
float 			toFloat(char * str);
long 			toLong(char * str);
unsigned long 	toULong(char * str);
unsigned short 	toUShort(char * str);
char * 			doubleToString(double val, signed char width = 10, unsigned char prec = 2);
char * 			floatToString(float val, signed char width = 10, unsigned char prec = 2);
char * 			longToString(long val, signed char width = 5);
char * 			uLongToString(unsigned long val, signed char width = 11);
char * 			uShortToString(unsigned short val, signed char width = 5);

#endif