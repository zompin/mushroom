/*
 * Библиотке для работы с NTC-термистором
 */

#if ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

class NTC {
	public: 
		NTC(byte);
		float getTemperature(void);
		void setTermistorResistance(long);
		void setResistance(long);
		void setBCoefficient(byte);

	private:
		byte _port = 0;
		long _resistance = 10000;
		long _termistorResistance = 10000;
		int _bcoefficient = 3950;
		byte _temperatureNominal = 25;
};