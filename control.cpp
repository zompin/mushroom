/*
 * Библиотека для управления устройствами на 220В через реле
 * Алтухов Илья
 */

#include "control.h"

// Функция-конструктор
// Аргументы:
// id 		- идентификатор контрола в базе
// paramId 	- идентификатор параметра в базе
// type 	- тип контрола
// port 	- порт к которому подключено реле
Control::Control(unsigned short id, unsigned short paramId, uint8_t type, uint8_t port, uint8_t timerEnable, uint32_t timerStart, uint32_t timerEnd, uint8_t timerDelay) {
	_id 			= id;
	_paramId 		= paramId;
	_type 			= type;
	_port 			= port;
	_timerEnable 	= timerEnable;
	_timerStart 	= timerStart;
	_timerEnd 		= timerEnd;
	_timerDelay 	= timerDelay;
	pinMode(_port, OUTPUT);
}

// Включает текущий контрол
void Control::turnOn() {
	_state = ON;
	digitalWrite(_port, HIGH);
}

// Выключает текущий контрол
void Control::turnOff() {
	//if (_state != OFF) {
		digitalWrite(_port, LOW);
	//}
	_state = OFF;
}

// Возращает идентификатор контрола
unsigned short Control::getControlId() {
	return _id;
}

// Возращает идентификатор параметра
unsigned short Control::getParamId() {
	return _paramId;
}

// Возращает тип контрола
uint8_t Control::getControlType() {
	return _type;
}

// Возращает номер порта
uint8_t Control::getPort() {
	return _port;
}

// Возращает текущее состояние контрола
uint8_t Control::getControlState() {
	return _state;
}

// Проверяет включен ли таймер
uint8_t Control::isTimerEnable() {
	return _timerEnable;
}

// Возвращает таймштамп начала работы таймера
uint32_t Control::getTimerStart() {
	return _timerStart;
}

// Возвращает таймштамп конца работы таймера
uint32_t Control::getTimerEnd() {
	return _timerEnd;
}

uint8_t Control::isTimerDelay() {
	return _timerDelay;
}

void Control::turnOffTimerDelay(uint32_t currentTime) {
	uint32_t offset = _timerEnd - _timerStart;

	if (currentTime > 0) {
		_timerDelay 	= 0;
		_timerStart 	= currentTime;
		_timerEnd 		= _timerStart + offset;
	}
}