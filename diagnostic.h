#ifndef __DIAGNOSTIC__
#define __DIAGNOSTIC__

extern int __bss_end;
extern void *__brkval;

int getFreeMemory();

#endif